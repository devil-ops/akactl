/*
Package main runs the CLI
*/
package main

import "gitlab.oit.duke.edu/devil-ops/akactl/cmd/akactl/cmd"

func main() {
	cmd.Execute()
}
