package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	aka "gitlab.oit.duke.edu/devil-ops/aka-sdk"

	"github.com/spf13/cobra"
)

// getIPCmd represents the getIP command
var getIPCmd = &cobra.Command{
	Use:     "ip IP_ADDR [IP_ADDR...]",
	Short:   "Get IP entry",
	Args:    cobra.MinimumNArgs(1),
	Aliases: []string{"ips"},
	Long:    `Get ips in AKA`,
	RunE: func(_ *cobra.Command, args []string) error {
		items := make([]*aka.IP, len(args))
		for idx, lookup := range args {
			item, err := client.ReadIP(context.Background(), lookup)
			if err != nil {
				return err
			}
			items[idx] = item
		}

		return gout.Print(items)
	},
}

func init() {
	getCmd.AddCommand(getIPCmd)
}
