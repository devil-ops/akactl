/*
Package cmd is the command line interface
*/
package cmd

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/charmbracelet/log"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk"

	"github.com/spf13/cobra"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	client  *aka.Client
	logger  *slog.Logger
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:           "akactl",
	Short:         "Do junk™️ with aka",
	SilenceErrors: true,
	SilenceUsage:  true,
	Long: `This app interacts with AKA similar to the way kubectl interacts
with Kubernetes.  Note, the proper pronunciation is "Ay Kay Ay Cuddle"

Make sure to export the following environment variables for this to auth

export AKA_USER=<your_netid>
export AKA_KEY=<your_aka_api_key>
`,
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		return goutbra.Cmd(cmd)
	},

	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		logger.Error("fatal error", "error", err)
		os.Exit(2)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// Initialize AKA client here
	client = aka.MustNew()

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.akactl.yaml)")

	if err := goutbra.Bind(rootCmd); err != nil {
		panic(err)
	}
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			panic(err)
		}

		// Search config in home directory with name ".akactl" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".akactl")
	}

	logger = slog.New(log.NewWithOptions(os.Stderr, log.Options{
		Prefix: "akactl 🥸 ",
	}))

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
