package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getZoneCmd represents the getZone command
var getZoneCmd = &cobra.Command{
	Use:     "zone",
	Short:   "Get Zones",
	Aliases: []string{"zones"},
	Args:    cobra.NoArgs,
	Long:    `Get zones in AKA`,
	RunE: func(_ *cobra.Command, _ []string) error {
		zones, err := client.ListZones(context.Background())
		if err != nil {
			return err
		}
		return gout.Print(zones)
	},
}

func init() {
	getCmd.AddCommand(getZoneCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getZoneCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getZoneCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
