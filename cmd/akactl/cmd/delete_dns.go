package cmd

import (
	"errors"

	"github.com/spf13/cobra"
)

var deleteDNSCmd = &cobra.Command{
	Use:   "dns",
	Args:  cobra.ExactArgs(1),
	Short: "Delete a DNS record",
	Long: `Delete a DNS record from AKA

Examples:

`,
	RunE: func(_ *cobra.Command, _ []string) error {
		return errors.New("not yet implemented")
	},
}

func init() {
	deleteCmd.AddCommand(deleteDNSCmd)
	// deleteCnameCmd.PersistentFlags().String("foo", "", "A help for foo")
	// deleteCnameCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
