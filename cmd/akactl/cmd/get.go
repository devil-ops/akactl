package cmd

import (
	"errors"

	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get ip|dns|subnzet|zone",
	Short: "Get objects from AKA",
	Long: `Examples:
$ akactl get dns <some_dns_entry>
$ akactl get ip <some_ip>
$ akactl get subnets
$ akactl get zones
`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, _ []string) error {
		return errors.New(cmd.UsageString())
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
