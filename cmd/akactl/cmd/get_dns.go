package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"

	"github.com/spf13/cobra"
)

// getDNSCmd represents the getDNS command
var getDNSCmd = &cobra.Command{
	Use:     "dns",
	Short:   "Get DNS entry",
	Args:    cobra.MinimumNArgs(1),
	Aliases: []string{"dnss"},
	Long:    `Get dnss in AKA`,
	RunE: func(_ *cobra.Command, args []string) error {
		var items []interface{}
		for _, lookup := range args {
			recs, err := client.GetDNSRecord(context.Background(), lookup)
			if err != nil {
				return err
			}
			for _, item := range recs {
				items = append(items, item)
			}
		}

		return gout.Print(items)
	},
}

func init() {
	getCmd.AddCommand(getDNSCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getDNSCmd.PersistentFlags().StringP("type", "t", "", "Filter by Type of record")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getDNSCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
