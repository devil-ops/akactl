package cmd

import (
	"context"

	"github.com/spf13/cobra"
)

// deleteCnameCmd represents the deleteCname command
var deleteCnameCmd = &cobra.Command{
	Use:   "cname",
	Args:  cobra.ExactArgs(1),
	Short: "Delete a CNAME",
	Long: `Delete a CNAME from AKA

Examples:

$ akactl delete cname foo-alias.oit.duke.edu
`,
	RunE: func(_ *cobra.Command, args []string) error {
		logger.Info("deleting", "host", args[0])
		return client.DeleteCNAME(context.Background(), args[0])
	},
}

func init() {
	deleteCmd.AddCommand(deleteCnameCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// deleteCnameCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// deleteCnameCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
