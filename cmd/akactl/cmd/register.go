package cmd

import (
	"context"
	"strings"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	aka "gitlab.oit.duke.edu/devil-ops/aka-sdk"
)

// registerCmd represents the register command
var registerCmd = &cobra.Command{
	Use:   "register HOSTNAME NETWORK|IP",
	Short: "Register a host in AKA",
	Long:  `Register a host in AKA.  This includes setting up the forward and reverse mapping that a standard host would get`,
	Args:  cobra.ExactArgs(2),
	Example: `Register a host with a specific IP
$ akactl register my-awesome-host-01.oit.duke.edu 1.2.3.4

Register a host to the next available IP on a specific network
$ akactl register my-superawesome-host-02.oit.duke.edu 1.2.3.4/24
`,
	RunE: func(_ *cobra.Command, args []string) error {
		if strings.Contains(args[1], "/") {
			logger.Info("registering next available ip", "target-space", args[1])
			res, err := client.RegisterNextAvailable(context.Background(), args[1], &aka.IPParams{
				Hostname: args[0],
				TTL:      3600,
			})
			if err != nil {
				return err
			}
			return gout.Print(res)
		}
		logger.Info("registering host", "target-space", args[1])
		res, err := client.CreateIP(context.Background(), args[1], &aka.IPParams{
			Hostname: args[0],
			TTL:      3600,
		})
		if err != nil {
			return err
		}
		return gout.Print(res)
	},
}

func init() {
	rootCmd.AddCommand(registerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// registerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// registerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
