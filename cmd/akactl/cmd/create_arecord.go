package cmd

import (
	"context"

	"github.com/spf13/cobra"
	aka "gitlab.oit.duke.edu/devil-ops/aka-sdk"
)

// createArecordCmd represents the createArecord command
var createArecordCmd = &cobra.Command{
	Use:     "arecord",
	Short:   "Create an A record",
	Long:    `Create an A record in AKA`,
	Args:    cobra.ExactArgs(2),
	Example: `$ akactl create arecord some-name.oit.duke.edu 1.2.3.4`,
	RunE: func(_ *cobra.Command, args []string) error {
		_, err := client.ReadIP(context.Background(), args[1])
		if err != nil {
			return err
		}
		_, err = client.CreateDNSRecord(context.Background(), aka.DNSParams{
			Name:    args[0],
			Type:    "A",
			TTL:     300,
			Address: args[1],
		})
		if err != nil {
			return err
		}
		logger.Info("created arecord!")
		return nil
	},
}

func init() {
	createCmd.AddCommand(createArecordCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createArecordCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createArecordCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
