package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// createCnameCmd represents the createCname command
var createCnameCmd = &cobra.Command{
	Use:   "cname",
	Args:  cobra.ExactArgs(2),
	Short: "Create a new CNAME",
	Long: `Create a CNAME in AKA

Example:

akactl create cname foo-alias.oit.duke.edu foo-host-01.oit.duke.edu
`,
	RunE: func(_ *cobra.Command, args []string) error {
		logger.Info("creating cname", "source", args[0], "target", args[1])
		rec, err := client.CreateCNAME(context.Background(), args[0], args[1])
		if err != nil {
			return err
		}
		return gout.Print(rec)
	},
}

func init() {
	createCmd.AddCommand(createCnameCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createCnameCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createCnameCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
