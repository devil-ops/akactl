package cmd

import (
	"context"

	"github.com/spf13/cobra"
)

// updateCnameCmd represents the updateCname command
var updateCnameCmd = &cobra.Command{
	Use:   "cname",
	Short: "Update a CNAME",
	Args:  cobra.ExactArgs(2),
	Long: `Update a CNAME record in AKA

$ akactl update cname foo-alias.oit.duke.edu foo-02.oit.duke.edu
`,
	RunE: func(_ *cobra.Command, args []string) error {
		logger.Info("update CNAME called")
		return client.UpdateCNAME(context.Background(), args[0], args[1])
	},
}

func init() {
	updateCmd.AddCommand(updateCnameCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// updateCnameCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// updateCnameCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
