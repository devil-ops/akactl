package cmd

import (
	"context"

	"github.com/drewstinnett/gout/v2"

	"github.com/spf13/cobra"
)

// getSubnetCmd represents the getSubnet command
var getSubnetCmd = &cobra.Command{
	Use:   "subnet [cidr|name]",
	Short: "Get Subnets", Aliases: []string{"subnets", "nets", "networks", "network"},
	Long: `Get subnets in AKA`,
	Args: cobra.MaximumNArgs(1),
	Example: `$ akactl get subnet
$ akactl get subnet 152.3.111.128/25
$ akactl get subnet OIT-INTL-VPN-POOL-PORTAL-2`,
	RunE: func(_ *cobra.Command, args []string) error {
		var items []interface{}
		subnets, err := client.ListSubnets(context.Background())
		if err != nil {
			return err
		}
		filter := ""
		if len(args) > 0 {
			filter = args[0]
		}
		for _, item := range *subnets {
			if filter != "" {
				switch {
				case item.Name == filter:
					items = append(items, item)
				case item.Cidr == filter:
					items = append(items, item)
				}
			} else {
				items = append(items, item)
			}
		}
		return gout.Print(items)
	},
}

func init() {
	getCmd.AddCommand(getSubnetCmd)
}
