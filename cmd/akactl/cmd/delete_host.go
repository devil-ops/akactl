package cmd

import (
	"context"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
)

var deleteHostCmd = &cobra.Command{
	Use:   "host",
	Args:  cobra.ExactArgs(1),
	Short: "Delete a Host record",
	Long: `Delete a Host record from AKA. This is actually deleting the PTR to the IP and the A record.

Examples:

$ akactl delete host drew-test-01.oit.duke.edu

`, RunE: func(_ *cobra.Command, args []string) error {
		r, err := client.GetDNSRecord(context.Background(), args[0])
		if err != nil {
			return err
		}
		resultLen := len(r)
		switch {
		case resultLen > 2:
			return fmt.Errorf("too many results, we expect either 1 or 2 (internal and possibly external) but got %v", resultLen)
		case resultLen == 0:
			return errors.New("no results found")
		}
		for _, item := range r {
			logger.Info("deleting", "host", args[0], "view", item.View)
			if _, err := client.DeleteDNSRecord(context.Background(), item.EditPath); err != nil {
				return err
			}
		}
		return nil
	},
}

func init() {
	deleteCmd.AddCommand(deleteHostCmd)
}
