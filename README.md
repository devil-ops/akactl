# akactl

Interact with AKA in a way similar to kubectl

This uses the [aka-sdk](https://gitlab.oit.duke.edu/devil-ops/aka-sdk) for AKA
operations

## Installation

Download a binary for your favorite OS from the [packages
page](https://gitlab.oit.duke.edu/devil-ops/akactl/-/packages)

If you want to pull it in with go instead, you can use:

```
go get gitlab.oit.duke.edu/devil-ops/akactl/akactl
```

## Using

Right now, you need a couple environment variables set to auth:

```
export AKA_USER=YOUR_NETID
export AKA_KEY=KEY_FROM_AKA
```

## Releasing

Tag code with a name matching v\d.\d.\d (Example: git tag v1.2.3).  This will
publish a new package on gitlab

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
